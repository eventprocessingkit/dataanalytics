# New York City Taxi Analyse in R


Dieses Repository ist im Rahmen des Seminars "Event Processing" des
Forschungszentrum Informatik (FZI) im Sommersemster 2016 am 
Karlsruher Institut für Technologie (KIT) entstanden.

Von Tim Bossenmaier, Nevena Nikolajevic und Adrian Vetter.


### Verwendung
1. Datensatz von http://www.debs2015.org/call-grand-challenge.html herunterladen.
2. _sorted_data.csv_ in das Verzeichnis _data_ legen (falls nicht vorhanden neu anlegen).
3. Projekt _DataAnalytics.Rproj_ öffnen.
4. R Dateien im _analytics_ Verzsichniss ausführen.